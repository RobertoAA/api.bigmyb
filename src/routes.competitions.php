<?php
// Routes

//competitions
// get all competitions
$app->get('/competitions', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM competitions ORDER BY id DESC");
    $sth->execute();
    $competitions = $sth->fetchAll();
    return $this->response->withJson($competitions);
});

//get competition with id
$app->get('/competition/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM competitions WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $competition = $sth->fetchObject();
    return $this->response->withJson($competition);
});

//get competition with name
$app->get('/competition/name/[{name}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM competitions WHERE name=:name");
    $sth->bindParam("name", $args['name']);
    $sth->execute();
    $competition = $sth->fetchObject();
    return $this->response->withJson($competition);
});

//get competition with country_id
$app->get('/competition/country/{country_id}', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM competitions WHERE country_id=:country_id");
    $sth->bindParam("country_id", $args['country_id']);
    $sth->execute();
    $competition = $sth->fetchObject();
    return $this->response->withJson($competition);
});

// Add a new competition
$app->post('/competition', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO competitions (name, country_id) 
            VALUES (:name, :country_id)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("country_id", $input['country_id']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a competition given id
$app->put('/competition/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE competitions SET name=:name, country_id=:country_id WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});