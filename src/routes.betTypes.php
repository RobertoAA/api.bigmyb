<?php
// Routes

//betTypes
// get all betTypes
$app->get('/betTypes', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM bet_types ORDER BY id DESC");
    $sth->execute();
    $betTypes = $sth->fetchAll();
    return $this->response->withJson($betTypes);
});

//get betType with id
$app->get('/betType/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM bet_types WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $betType = $sth->fetchObject();
    return $this->response->withJson($betType);
});

//get betType with name
$app->get('/betType/name/[{name}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM bet_types WHERE name=:name");
    $sth->bindParam("name", $args['name']);
    $sth->execute();
    $betType = $sth->fetchObject();
    return $this->response->withJson($betType);
});

// Add a new betType
$app->post('/betType', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO bet_types (name, options, sport_id) 
            VALUES (:name, :options, :sport_id)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("options", $input['options']);
    $sth->bindParam("sport_id", $input['sport_id']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a betType given id
$app->put('/betType/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE bet_types SET name=:name, options=:options, sport_id=:sport_id WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("options", $input['options']);
    $sth->bindParam("sport_id", $input['sport_id']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});