<?php
// Routes

//odds
// get all odds
$app->get('/odds[/{extra}]', function ($request, $response, $args) {
    if(!empty($args['extra'])){
        $sql = "SELECT odds.`*`, t1.name AS home_team_name, t2.name AS away_team_name, bet_types.name AS bet_type_name, sportsbooks.name AS sportsbook_name
        FROM odds
        INNER JOIN matches ON matches.id = odds.match_id
        INNER JOIN teams AS t1 ON t1.id = matches.home_team_id
        INNER JOIN teams AS t2 ON t2.id = matches.away_team_id
        INNER JOIN bet_types ON bet_types.id = odds.bet_type_id
        INNER JOIN sportsbooks ON sportsbooks.id = odds.sportsbook_id
        ORDER BY DATETIME DESC";
    }else{
        $sql = "SELECT * FROM odds ORDER BY DATETIME DESC";
    }
    $sth = $this->db->prepare($sql);
    $sth->execute();
    $matches = $sth->fetchAll();
    return $this->response->withJson($matches);
});

//get odd with id
$app->get('/odd/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM odds WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $match = $sth->fetchObject();
    return $this->response->withJson($match);
});

//get odd with match_id    
$app->get('/odds/search/match/[{match_id}]', function ($request, $response, $args) {
    $sql = "SELECT odds.`*`, t1.name AS home_team_name, t2.name AS away_team_name, bet_types.name AS bet_type_name, sportsbooks.name AS sportsbook_name
    FROM odds
    INNER JOIN matches ON matches.id = odds.match_id
    INNER JOIN teams AS t1 ON t1.id = matches.home_team_id
    INNER JOIN teams AS t2 ON t2.id = matches.away_team_id
    INNER JOIN bet_types ON bet_types.id = odds.bet_type_id
    INNER JOIN sportsbooks ON sportsbooks.id = odds.sportsbook_id
    WHERE match_id=:match_id
    ORDER BY DATETIME DESC";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("match_id", $args['match_id']);
    $sth->execute();
    $match = $sth->fetchAll();
    return $this->response->withJson($match);
});


// Add a new odd
$app->post('/odd', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO odds (match_id, bet_type_id, value, datetime, sportsbook_id, result) 
            VALUES (:match_id, :bet_type_id, value, :datetime, :sportsbook_id, :result)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("match_id", $input['match_id']);
    $sth->bindParam("bet_type_id", $input['bet_type_id']);
    $sth->bindParam("value", $input['value']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("sportsbook_id", $input['sportsbook_id']);
    $sth->bindParam("result", $input['result']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Update a odd given id
$app->put('/odd/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE odds SET match_id=:match_id, bet_type_id=:bet_type_id, value=:value, datetime=:datetime, sportsbook_id=:sportsbook_id, result=:result WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("match_id", $input['match_id']);
    $sth->bindParam("bet_type_id", $input['bet_type_id']);
    $sth->bindParam("value", $input['value']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("sportsbook_id", $input['sportsbook_id']);
    $sth->bindParam("result", $input['result']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});

// Delete a odd given id
$app->delete('/odd/[{id}]', function ($request, $response, $args) {
    $sql = "DELETE FROM odds WHERE id=:id"; 
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $args['id']);
    $this->logger->info($sql);
    $sth->execute();
    return $this->response->withJson($args);
});