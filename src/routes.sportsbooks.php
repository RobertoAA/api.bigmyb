<?php
// Routes

//sportsbooks
// get all sportsbooks
$app->get('/sportsbooks', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sportsbooks ORDER BY id DESC");
    $sth->execute();
    $sportsbooks = $sth->fetchAll();
    return $this->response->withJson($sportsbooks);
});

//get sportsbook with id
$app->get('/sportsbook/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sportsbooks WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $sportsbook = $sth->fetchObject();
    return $this->response->withJson($sportsbook);
});

//get sportsbook with name
$app->get('/sportsbook/name/[{name}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sportsbooks WHERE name=:name");
    $sth->bindParam("name", $args['name']);
    $sth->execute();
    $sportsbook = $sth->fetchObject();
    return $this->response->withJson($sportsbook);
});

// Add a new sportsbook
$app->post('/sportsbook', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO sportsbooks (name) 
            VALUES (:name)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a sportsbook given id
$app->put('/sportsbook/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE sportsbooks SET name=:name WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});