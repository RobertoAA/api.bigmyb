<?php
// Routes

/*
    {
        "sportsbook": "marathon",
        "sportsbook_id": 1,
        "sport": "soccer",
        "sport_id": 1,
        "season_id": "",
        "competition": "Liga",
        "year_start": 2018,
        "year_end": 2019,
        "datetime": "2017-11-12 17:58:23",
        "date_range": "2",
        "insert_match": 1,
        "matches":[
            {
                "home_team": "Real Madrid",
                "away_team": "Barcelona",
                "datetime": "2017-11-12 22:00",
                "odds": [
                    {
                        "bet_type": "Match_winner",
                        "bet_type_id": 1,
                        "value": "[2.3, 3.2, 3.8]",
                        "datetime": ""
                    }
                ]
            },
            {
                "home_team": "Betis",
                "away_team": "Getafe",
                "datetime": "2017-11-12 21:00",
                "season_id": "",
                "competition": "liga",
                "year_start": 2018,
                "year_end": 2019,
                "odds": [
                    {
                        "bet_type": "Match_winner",
                        "bet_type_id": 1,
                        "value": "[2.3, 3.2, 3.8]",
                        "datetime": ""
                    }
                ]
            }
        ]
    }
*/

// Add a new scrapped
$app->post('/scrapped', function ($request, $response) {
    $input = $request->getParsedBody();
    if(!empty($input['data'])){
        $data = json_decode($input['data'], true);
    };
    if(empty($data)){
        $data = (array) json_decode(file_get_contents('php://input'), TRUE);
    }
    $response = array(
        'messages' => array()
    );
    if( empty($data['matches']) ){
        $response['messages'] = 'Matches empty';
        return $this->response->withJson($response);
    };
    foreach($data['matches'] as $match){
        $response['messages'][]='Search home team id: ' . $match['home_team'];
        $match['home_team_object'] = getTeam($match['home_team'], $data['sport_id'], $this);
        if( !empty($match['home_team_object']) ){
            $response['messages'][]='Found: ' . $match['home_team_object']->id;
        }else{
            $response['messages'][]='Not found';
        }

        $response['messages'][]='Search away team id: ' . $match['away_team'];
        $match['away_team_object'] = getTeam($match['away_team'], $data['sport_id'], $this);
        if( !empty($match['away_team_object']) ){
            $response['messages'][] = 'Found: ' . $match['away_team_object']->id;
        }else{
            $response['messages'][] = 'Not found';
        }

        // match id
        if( !empty($match['home_team_object']->id) && !empty($match['away_team_object']->id) ){
            $response['messages'][] = 'Search match id on : ' . $match['datetime'];
            $this->logger->info('Search match id on : ' . $match['datetime']);
            $season_id = (!empty($match['season_id']))? $match['season_id'] : $data['season_id'];
            if(!empty($match['competition']) && empty($season_id)){
                $season = getSeason($match['competition'], $match['year_start'], $match['year_end'], $this);
                if(empty($season)){
                    $season_id = false;
                    $response['messages'][] = 'Season not found: ' . $match['competition'] . ' year start: ' . $match['year_start'];
                    $this->logger->info('Season not found: ' . $match['competition'] . ' year start: ' . $match['year_start']);
                }else{
                    $match['competition_id'] = $competition->id;
                    $competition_id = $match['competition_id'];
                }
            }
            if($season_id){
                $match['match_object'] = getMatch($match['home_team_object']->id, $match['away_team_object']->id, $match['datetime'], $data['date_range'], $season_id, $this);        
                if( !empty($match['match_object']) ){
                    $match['id'] = $match['match_object']->id;
                    $response['messages'][] = 'Found: ' . $match['id'];
                    $this->logger->info('Found: ' . $match['id']);
                }else{
                    $response['messages'][] = 'Not found';
                    $this->logger->info('Not found');
                    if($data['insert_match'] == 1){
                        $match['id'] = insertMatch($match['home_team_object']->id, $match['away_team_object']->id, $match['datetime'], $season_id, $this);
                        $response['messages'][] = 'Add: ' . $match['id'];
                        $this->logger->info('Add: ' . $match['id']);
                    }
                }
            }
        }

        // odds
        if( !empty($match['id']) ){
            foreach($match['odds'] as $odd){
                $response['messages'][]='Search odd id on match ' . $match['id'] . ' with bet type id: ' . $odd['bet_type_id'];
                $odds['odd_object'] = getOdds($odd['bet_type_id'], $match['id'], $data['sportsbook_id'], $this);
                if( !empty($odds['odd_object']) ){
                    $odd['id'] = $odds['odd_object'][0]['id'];
                    $response['messages'][] = 'Found and compare values of odd id: ' . $odd['id'];
                    if( $odds['odd_object'][0]['value'] === $odd['value']){
                        $response['messages'][] = 'Equal value';
                        $response['messages'][] = 'Updating date';
                        updateOdd( $odd['id'], $data['datetime'], $this);
                    }else{
                        $response['messages'][] = 'Different value. Insert ';
                        insertOdd($match['id'], $odd['bet_type_id'], $odd['value'], $data['datetime'], $data['sportsbook_id'], $this);
                    }
                }else{
                    $this->logger->info('Not found');
                    $response['messages'][] = 'Not found';
                    $odd['id'] = insertOdd($match['id'], $odd['bet_type_id'], $odd['value'], $data['datetime'], $data['sportsbook_id'], $this);
                    $response['messages'][] = 'Add: ' . $odd['id'];
                }
            }
        }
    }

    return $this->response->withJson($response);
});

function getTeam($name, $sportId, $app){
    $sth = $app->db->prepare("SELECT * FROM teams WHERE name=:name AND sport_id=:sport_id");
    $sth->bindParam("name", $name);
    $sth->bindParam("sport_id", $sportId);
    $sth->execute();
    $team = $sth->fetchObject();

    if(empty($team)){
        $sth = $app->db->prepare("SELECT teams.* FROM alias_teams INNER JOIN teams ON teams.id=alias_teams.team_id WHERE alias_teams.name=:name AND teams.sport_id=:sport_id");
        $sth->bindParam("name", $name);
        $sth->bindParam("sport_id", $sportId);
        $sth->execute();
        $alias_team = $sth->fetchObject();
        $team = $alias_team;
    }

    return $team;
}

function getSeason($competition_name, $year_start, $year_end, $app){
    $sth = $app->db->prepare("SELECT * FROM seasons WHERE competition_id = c.id AND year_start=:year_start AND year_end:=year_end INNER JOIN competitions AS c ON name=:name");
    $sth->bindParam("name", $competition_name);
    $sth->bindParam("year_start", $year_start);
    $sth->bindParam("year_end", $year_end);
    $sth->execute();
    $season = $sth->fetchObject();
    return $season;
}

function insertCompetition($competition_name, $app){
    $sth = $app->db->prepare("INSERT INTO competitions (name) VALUES(:name)");
    $sth->bindParam("name", $competition_name);
    $sth->execute();
    return $app->db->lastInsertId();
}

function getMatch($home_team_id, $away_team_id, $datetime, $date_range, $season_id, $app){
    $low_datetime = date("Y-m-d H:i:s", strtotime($datetime . ' - ' . $date_range . ' days'));
    $high_datetime = date("Y-m-d H:i:s", strtotime($datetime . ' + ' . $date_range . ' days'));
    $sth = $app->db->prepare("SELECT * FROM matches WHERE home_team_id=:home_team_id AND away_team_id=:away_team_id AND season_id=:season_id AND datetime BETWEEN :low_datetime AND :high_datetime");
    $sth->bindParam("home_team_id", $home_team_id);
    $sth->bindParam("away_team_id", $away_team_id);
    $sth->bindParam("season_id", $season_id);
    $sth->bindParam("low_datetime", $low_datetime);
    $sth->bindParam("high_datetime", $high_datetime);
    $sth->execute();
    $match = $sth->fetchObject();
    return $match;
}

function insertMatch($home_team_id, $away_team_id, $datetime, $season_id, $app){
    $sth = $app->db->prepare("INSERT INTO matches (home_team_id, away_team_id, datetime, season_id) VALUES (:home_team_id, :away_team_id, :datetime, :season_id)");
    $sth->bindParam("home_team_id", $home_team_id);
    $sth->bindParam("away_team_id", $away_team_id);
    $sth->bindParam("datetime", $datetime);
    $sth->bindParam("season_id", $season_id);
    $sth->execute();
    return $app->db->lastInsertId();
}

function getOdds($bet_type_id, $match_id, $sportsbook_id, $app){
    $sth = $app->db->prepare("SELECT * FROM odds WHERE bet_type_id=:bet_type_id AND match_id=:match_id AND sportsbook_id=:sportsbook_id ORDER BY datetime DESC");
    $sth->bindParam("bet_type_id", $bet_type_id);
    $sth->bindParam("match_id", $match_id);
    $sth->bindParam("sportsbook_id", $sportsbook_id);
    $sth->execute();
    $odds = $sth->fetchAll();
    return $odds;
}

function insertOdd($match_id, $bet_type_id, $value, $datetime, $sportsbook_id, $app){
    $sth = $app->db->prepare("INSERT INTO odds (match_id, bet_type_id, value, datetime, sportsbook_id) VALUES (:match_id, :bet_type_id, :value, :datetime, :sportsbook_id)");
    $sth->bindParam("match_id", $match_id);
    $sth->bindParam("bet_type_id", $bet_type_id);
    $sth->bindParam("value", $value);
    $sth->bindParam("datetime", $datetime);
    $sth->bindParam("sportsbook_id", $sportsbook_id);
    $sth->execute();
    return $app->db->lastInsertId();
};

function updateOdd($id, $datetime, $app){
    $sql = "UPDATE odds SET datetime_last=:datetime WHERE id=:id";
    $sth = $app->db->prepare($sql);
    $sth->bindParam("id", $id);
    $sth->bindParam("datetime", $datetime);
    $sth->execute();
    return $app->response->withJson($id);
}