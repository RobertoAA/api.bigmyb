<?php
// Routes

//countries
// get all countries
$app->get('/countries', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM countries ORDER BY id DESC");
    $sth->execute();
    $countries = $sth->fetchAll();
    return $this->response->withJson($countries);
});

//get country with id
$app->get('/country/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM countries WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $country = $sth->fetchObject();
    return $this->response->withJson($country);
});

// Add a new country
$app->post('/country', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO countries (code, name) 
            VALUES (:code, :name)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("code", $input['code']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a country given id
$app->put('/country/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE countries SET code=:code, name=:name WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("code", $input['code']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});