<?php
// Routes

//sports
// get all sports
$app->get('/sports', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sports ORDER BY id DESC");
    $sth->execute();
    $sports = $sth->fetchAll();
    return $this->response->withJson($sports);
});

//get sport with id
$app->get('/sport/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sports WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $sport = $sth->fetchObject();
    return $this->response->withJson($sport);
});

//get sport with name
$app->get('/sport/name/[{name}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM sports WHERE name=:name");
    $sth->bindParam("name", $args['name']);
    $sth->execute();
    $sport = $sth->fetchObject();
    return $this->response->withJson($sport);
});

// Add a new sport
$app->post('/sport', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO sports (name) 
            VALUES (:name)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a sport given id
$app->put('/sport/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE sports SET name=:name WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});