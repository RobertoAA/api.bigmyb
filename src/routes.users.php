<?php
// Routes

//users
// get all users
$app->get('/users', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM users ORDER BY id DESC");
    $sth->execute();
    $users = $sth->fetchAll();
    return $this->response->withJson($users);
});

//get user with id
$app->get('/user/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM users WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $user = $sth->fetchObject();
    return $this->response->withJson($user);
});

// Add a new user
$app->post('/user', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO users (name) 
            VALUES (:name)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a user given id
$app->put('/user/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE users SET name=:name WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});