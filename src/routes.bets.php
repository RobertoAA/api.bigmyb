<?php
// Routes

//bets
// get all bets
$app->get('/bets', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM bets ORDER BY id DESC");
    $sth->execute();
    $matches = $sth->fetchAll();
    return $this->response->withJson($matches);
});

//get bet with id
$app->get('/bet/[{id}]', function ($request, $response, $args) {
         $sth = $this->db->prepare("SELECT * FROM bets WHERE id=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $match = $sth->fetchObject();
        return $this->response->withJson($match);
    });

//get bets with user_id  
/*  Developing user_id??
$app->get('/odds/search/user/[{user_id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM bets WHERE user_id=:user_id");
    $sth->bindParam("user_id", $args['user_id']);
    $sth->execute();
    $match = $sth->fetchObject();
    return $this->response->withJson($match);
});
*/

// Add a new bet
$app->post('/bet', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO bets (odd_id, datetime, option_id, import, result, account_id) 
            VALUES (:odd_id, :datetime, :option_id, :import, :result, :account_id)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("odd_id", $input['odd_id']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("option_id", $input['option_id']);
    $sth->bindParam("import", $input['import']);
    $sth->bindParam("result", $input['result']);
    $sth->bindParam("account_id", $input['account_id']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Update a bet given id
$app->put('/bet/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE bets SET odd_id=:odd_id, datetime=:datetime, option_id=:option_id, import=:import, result=:result, account_id=:account_id WHERE id=:id"; 
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("odd_id", $input['odd_id']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("option_id", $input['option_id']);
    $sth->bindParam("import", $input['import']);
    $sth->bindParam("result", $input['result']);
    $sth->bindParam("account_id", $input['account_id']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});