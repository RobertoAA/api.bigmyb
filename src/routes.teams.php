<?php
// Routes

//teams
// get all teams
$app->get('/teams', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM teams ORDER BY id DESC");
    $sth->execute();
    $teams = $sth->fetchAll();
    return $this->response->withJson($teams);
});

//get team with id
$app->get('/team/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM teams WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $team = $sth->fetchObject();
    return $this->response->withJson($team);
});

// Add a new team
$app->post('/team', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO teams (name, sport_id, country_id) 
            VALUES (:name, :sport_id, :country_id)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("sport_id", $input['sport_id']);
    $sth->bindParam("country_id", $input['country_id']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a team given id
$app->put('/team/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE teams SET name=:name, sport_id=:sport_id, country_id=:country_id WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("sport_id", $input['sport_id']);
    $sth->bindParam("country_id", $input['country_id']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});

//Get with multiple optional params
$app->get('/teams/search/{call:.+}', function($request, $response, $args) {
    $keyValues = explode('/', $args['call']);
    $sql = "SELECT * FROM teams";
    for($i=0; $i<count($keyValues); $i=$i+2){
        $conditional = ($i===0)? " WHERE ": " AND ";
        $sql .= $conditional . $keyValues[$i] . "=" . $keyValues[$i+1];
    }
    $sql .= " ORDER BY id DESC";
    $sth = $this->db->prepare($sql);
    $this->logger->info($sql);
    $sth->execute();
    $teams = $sth->fetchAll();
    return $this->response->withJson($teams);
});