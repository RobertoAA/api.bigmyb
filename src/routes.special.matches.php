<?php

//special matches
// get all matches
$app->get('/special/matches/season/{season_id}', function ($request, $response, $args) {
    $sql = 'SELECT matches.id, matches.datetime, t1.name AS home_team_name, t2.name AS away_team_name, c.name
        FROM matches
        INNER JOIN teams AS t1 ON t1.id = matches.home_team_id
        INNER JOIN teams AS t2 ON t2.id = matches.away_team_id
        INNER JOIN seasons AS s ON s.id = matches.season_id
        WHERE season_id=?';
    $sth = $this->db->prepare($sql);
    $sth->execute([$args['season_id']]);
    $matches = $sth->fetchAll();
    $responseArray = array();
    foreach($matches as $match) {
        $sql = 'SELECT odds.value, odds.datetime, odds.datetime_last, sb.name as sportsbook_name, bt.name as bet_type_name, bt.options
            FROM odds
            INNER JOIN sportsbooks AS sb ON sb.id = odds.sportsbook_id
            INNER JOIN bet_types AS bt ON bt.id = odds.bet_type_id
            WHERE match_id = ?';    
        $sth = $this->db->prepare($sql);
        $sth->execute([$match['id']]);
        $odds = $sth->fetchAll();
        $m_bet_types = array_unique(array_map('getBetType', $odds));
        $i=0;
        foreach($m_bet_types as $bt){
            foreach($odds as $odd){
                if($odd['bet_type_name'] == $bt){
                    $match['bet_types'][$i]['bet_type_name'] = $bt;
                    $match['bet_types'][$i]['options'] = $odd['options'];
                    if(empty($match['bet_types'][$i]['sportsbooks'])){
                        $match['bet_types'][$i]['sportsbooks'] = array();
                    }
                    $indexSportbook = getIndex($match['bet_types'][$i]['sportsbooks'], 'sportsbook_name', $odd['sportsbook_name']);
                    if($indexSportbook === false){
                        $match['bet_types'][$i]['sportsbooks'][] = array(
                            'sportsbook_name' => $odd['sportsbook_name'],
                            'odds' => array(
                                array(
                                    'value' => $odd['value'],
                                    'datetime' => $odd['datetime'],
                                    'datetime_last' => $odd['datetime_last']
                                )
                            )
                        );
                    }else{
                        $match['bet_types'][$i]['sportsbooks'][$indexSportbook]['odds'][] = array(
                            'value' => $odd['value'],
                            'datetime' => $odd['datetime'],
                            'datetime_last' => $odd['datetime_last']
                        );
                    }
                }
            }
            $i++;
        }
        $responseArray[] = $match;
    }
    return $this->response->withJson($responseArray);
});
function getIndex($array, $property, $value){
    $i=0;
    foreach($array as $element){
        if(!empty($element[$property]) && $element[$property] === $value){
            return $i;
        }
        $i++;
    }
    return false;
}
function getBetType($odd){
    return $odd['bet_type_name'];
}

$app->get('/special/match/{id}[/{filter}/{value}]', function ($request, $response, $args) {
    $sql = 'SELECT * FROM matches WHERE id= ?';
    $sth = $this->db->prepare($sql);
    $sth->execute([$args['id']]);
    $result = $sth->fetchAll();
    var_dump($result);
});