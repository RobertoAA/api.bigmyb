<?php
// Routes

//accounts
// get all accounts
$app->get('/accounts', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM accounts ORDER BY id DESC");
    $sth->execute();
    $accounts = $sth->fetchAll();
    return $this->response->withJson($accounts);
});

//get account with id
$app->get('/account/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM accounts WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $account = $sth->fetchObject();
    return $this->response->withJson($account);
});

// Add a new account
$app->post('/account', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO accounts (user_id, sportsbook_id) 
            VALUES (:user_id, :sportsbook_id)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("user_id", $input['user_id']);
    $sth->bindParam("sportsbook_id", $input['sportsbook_id']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a account given id
$app->put('/account/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE accounts SET user_id=:user_id, sportsbook_id=:sportsbook_id WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("user_id", $input['user_id']);
    $sth->bindParam("sportsbook_id", $input['sportsbook_id']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});