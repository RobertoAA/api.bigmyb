<?php
// Routes

//matches
// get all matches
$app->get('/matches[/{extra}]', function ($request, $response, $args) {
    if(!empty($args['extra'])){
        $query = 'SELECT matches.*, t1.name AS home_team_name, t2.name AS away_team_name, c.name AS competition_name FROM matches 
        LEFT JOIN teams AS t1 ON matches.home_team_id=t1.id 
        LEFT JOIN teams AS t2 ON matches.away_team_id=t2.id 
        LEFT JOIN seasons AS s ON matches.season_id=s.id 
        LEFT JOIN competitions AS c ON s.competition_id=c.id 
        ORDER BY id DESC';
    }else{
        $query = 'SELECT * FROM matches ORDER BY id DESC';
    }
    $sth = $this->db->prepare($query);
    $sth->execute();
    $matches = $sth->fetchAll();
    return $this->response->withJson($matches);
});

//get match with id
$app->get('/match/[{id}]', function ($request, $response, $args) {
        $sql = "SELECT matches.*, t1.name AS home_team_name, t2.name AS away_team_name, c.name AS competition_name FROM matches 
        LEFT JOIN teams AS t1 ON matches.home_team_id=t1.id 
        LEFT JOIN teams AS t2 ON matches.away_team_id=t2.id 
        LEFT JOIN seasons AS s ON matches.season_id=s.id 
        LEFT JOIN competitions AS c ON s.competition_id=c.id 
        WHERE matches.id=:id 
        ORDER BY id DESC";
        $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $match = $sth->fetchObject();
        return $this->response->withJson($match);
    });

// Add a new match
$app->post('/match', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO matches (home_team_id, away_team_id, datetime, season_id, result) 
            VALUES (:home_team_id, :away_team_id, :datetime, :season_id, :result)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("home_team_id", $input['home_team_id']);
    $sth->bindParam("away_team_id", $input['away_team_id']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("season_id", $input['season_id']);
    $sth->bindParam("result", $input['result']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

// Update a match given id
$app->put('/match/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE matches SET home_team_id=:home_team_id, away_team_id=:away_team_id, datetime=:datetime, season_id=:season_id, result=:result WHERE id=:id"; 
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("home_team_id", $input['home_team_id']);
    $sth->bindParam("away_team_id", $input['away_team_id']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("season_id", $input['season_id']);
    $sth->bindParam("result", $input['result']);
    $sth->execute();
    $input['id'] = $args['id'];
    checkOdds($input, $this);
    return $this->response->withJson($input);
});

// Delete a match given id
$app->delete('/match/[{id}]', function ($request, $response, $args) {
    $sql = "DELETE FROM matches WHERE id=:id"; 
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $args['id']);
    $this->logger->info($sql);
    $sth->execute();
    return $this->response->withJson($args);
});

function checkOdds($input, $app){
    if(!empty($input['result'])){
        $sql = 'SELECT odds.`*`, bet_types.name AS bet_type_name FROM odds LEFT JOIN bet_types ON bet_types.id = odds.bet_type_id WHERE match_id= ?';
        $sth = $app->db->prepare($sql);
        $sth->execute([$input['id']]);
        $result = $sth->fetchAll();
        foreach($result as $row)
        {
            switch($row['bet_type_name']){
                case 'Match_winner':
                    $scores = explode( '-', $input['result']);
                    if($scores[0]>$scores[1]){
                        $valueResult = 1;
                    }elseif($scores[0]<$scores[1]){
                        $valueResult = 3;
                    }else{
                        $valueResult = 2; 
                    }
                    $row['result'] = $valueResult;
                    updateOdds($row, $app);
                    break;
            };
        }
    }
}

function updateOdds($input, $app){
    $sql = "UPDATE odds SET match_id=:match_id, bet_type_id=:bet_type_id, value=:value, datetime=:datetime, sportsbook_id=:sportsbook_id, result=:result WHERE id=:id";
    $sth = $app->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("match_id", $input['match_id']);
    $sth->bindParam("bet_type_id", $input['bet_type_id']);
    $sth->bindParam("value", $input['value']);
    $sth->bindParam("datetime", $input['datetime']);
    $sth->bindParam("sportsbook_id", $input['sportsbook_id']);
    $sth->bindParam("result", $input['result']);
    $sth->execute();
}