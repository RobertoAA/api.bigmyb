<?php
// Routes

//aliasTeams
// get all aliasTeams
$app->get('/aliasTeams', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM alias_teams ORDER BY id DESC");
    $sth->execute();
    $aliasTeams = $sth->fetchAll();
    return $this->response->withJson($aliasTeams);
});

//get aliasTeam with id
$app->get('/aliasTeam/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM alias_teams WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $aliasTeam = $sth->fetchObject();
    return $this->response->withJson($aliasTeam);
});

// Add a new aliasTeam
$app->post('/aliasTeam', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO alias_teams (team_id, name, web) 
            VALUES (:team_id, :name, :web)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("team_id", $input['team_id']);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("web", $input['web']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a aliasTeam given id
$app->put('/aliasTeam/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE alias_teams SET team_id=:team_id, name=:name, web=:web WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("team_id", $input['team_id']);
    $sth->bindParam("name", $input['name']);
    $sth->bindParam("web", $input['web']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});