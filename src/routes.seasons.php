<?php
// Routes

//seasons
// get all seasons
$app->get('/seasons', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM seasons ORDER BY id DESC");
    $sth->execute();
    $seasons = $sth->fetchAll();
    return $this->response->withJson($seasons);
});

//get season with id
$app->get('/season/[{id}]', function ($request, $response, $args) {
    $sth = $this->db->prepare("SELECT * FROM seasons WHERE id=:id");
    $sth->bindParam("id", $args['id']);
    $sth->execute();
    $season = $sth->fetchObject();
    return $this->response->withJson($season);
});

//get season with competition_id and year_start
$app->get('/season/competition/{competition_id}[/year_start/{year_start}]', function ($request, $response, $args) {
    $query = "SELECT * FROM seasons WHERE competition_id=:competition_id";
    if(!empty($argss['year_start'])){
        $query .= "AND year_start=:year_start";
    }
    $sth = $this->db->prepare($query);
    $sth->bindParam("competition_id", $args['competition_id']);
    if(!empty($argss['year_start'])){
        $sth->bindParam("year_start", $args['year_start']);
    }
    $sth->execute();
    $season = $sth->fetchObject();
    return $this->response->withJson($season);
});

// Add a new season
$app->post('/season', function ($request, $response) {
    $input = $request->getParsedBody();
    $sql = "INSERT INTO seasons (competition_id, year_start, year_end) 
            VALUES (:competition_id, :year_start, :year_end)";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("competition_id", $input['competition_id']);
    $sth->bindParam("year_start", $input['year_start']);
    $sth->bindParam("year_end", $input['year_end']);
    $sth->execute();
    $input['id'] = $this->db->lastInsertId();
    return $this->response->withJson($input);
});

//Update a season given id
$app->put('/season/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE seasons SET competition_id=:competition_id, year_start=:year_start, year_end=:year_end WHERE id=:id";
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id", $input['id']);
    $sth->bindParam("competition_id", $input['competition_id']);
    $sth->bindParam("year_start", $input['year_start']);
    $sth->bindParam("year_end", $input['year_end']);
    $sth->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});