<?php

date_default_timezone_set('Europe/Madrid');

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// CROSS
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");

// Register routes
require __DIR__ . '/../src/routes.accounts.php';
require __DIR__ . '/../src/routes.aliasTeams.php';
require __DIR__ . '/../src/routes.bets.php';
require __DIR__ . '/../src/routes.betTypes.php';
require __DIR__ . '/../src/routes.competitions.php';
require __DIR__ . '/../src/routes.countries.php';
require __DIR__ . '/../src/routes.matches.php';
require __DIR__ . '/../src/routes.odds.php';
require __DIR__ . '/../src/routes.seasons.php';
require __DIR__ . '/../src/routes.sports.php';
require __DIR__ . '/../src/routes.sportsbooks.php';
require __DIR__ . '/../src/routes.teams.php';
require __DIR__ . '/../src/routes.users.php';
require __DIR__ . '/../src/routes.default.php';

require __DIR__ . '/../src/routes.special.matches.php';

require __DIR__ . '/../src/routes.scrapped.php';
// Run app
$app->run();
